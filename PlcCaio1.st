PROGRAM program0
  VAR
    saida AT %QX100.0 : BOOL;
    setpoint AT %QW4 : UINT;
    Luminosidade AT %IW100 : UINT;
    SaidaAnalog AT %QW100 : UINT;
    Erro AT %QW20 : INT;
    aux1 AT %MW200 : UINT;
    aux2 AT %MX202.0 : BOOL;
  END_VAR
  VAR
    UINT_TO_INT26_OUT : INT;
    UINT_TO_INT27_OUT : INT;
    SUB20_OUT : INT;
    LT28_OUT : BOOL;
    MOVE30_ENO : BOOL;
    MOVE30_OUT : INT;
    INT_TO_UINT33_OUT : UINT;
    MUL25_OUT : UINT;
    GT14_OUT : BOOL;
  END_VAR

  UINT_TO_INT26_OUT := UINT_TO_INT(setpoint);
  UINT_TO_INT27_OUT := UINT_TO_INT(Luminosidade);
  SUB20_OUT := SUB(UINT_TO_INT26_OUT, UINT_TO_INT27_OUT);
  Erro := SUB20_OUT;
  LT28_OUT := LT(Erro, 0);
  MOVE30_OUT := MOVE(EN := LT28_OUT, IN := 0, ENO => MOVE30_ENO);
  IF MOVE30_ENO THEN
      Erro := MOVE30_OUT;
  END_IF;
  INT_TO_UINT33_OUT := INT_TO_UINT(Erro);
  MUL25_OUT := MUL(2, INT_TO_UINT33_OUT);
  aux1 := MUL25_OUT;
  GT14_OUT := GT(Luminosidade, setpoint);
  aux2 := GT14_OUT;
  SaidaAnalog := aux1;
  saida := aux2;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : program0;
  END_RESOURCE
END_CONFIGURATION
